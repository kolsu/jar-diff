# jar-diff

Simple tool to compare contents of two jar files.  I created it to aid me in moving from Ant to Maven build tool.

## Getting Started
Build project using Maven:
> mvn package

To Run:
>java -jar jar-diff-1.0.jar _jar1_ _jar2_


## Built With

 - Kotlin
 - Maven

## License
[Unlicense](http://unlicense.org)
