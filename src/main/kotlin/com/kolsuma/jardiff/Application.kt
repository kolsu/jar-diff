package com.kolsuma.jardiff

import java.util.*
import kotlin.collections.ArrayList


fun main(args: Array<String>) {

    val jarFile1 = args[0]
    val jarFile2 = args[1]

    val jar1Hashes = hashes(jarFile1)
    val jar2Hashes = hashes(jarFile2)

    val filesOnlyInJar1 = ArrayList<String>()
    val filesOnlyInJar2 = ArrayList<String>()
    val filesThatDiffer = ArrayList<String>()

    jar1Hashes.forEach({ name, hash ->
        if (jar2Hashes.containsKey(name)) {
            if (!Arrays.equals(hash, jar2Hashes[name])) {
                filesThatDiffer.add(name)
            }
        } else {
            filesOnlyInJar1.add(name)
        }
    })

    jar2Hashes.forEach({ name, _ ->
        if (!jar1Hashes.containsKey(name)) {
            filesOnlyInJar2.add(name)
        }
    })

    print(filesOnlyInJar1, jarFile1)
    print(filesOnlyInJar2, jarFile2)
    print(filesThatDiffer)
}

private fun print(filesOnlyInJar: ArrayList<String>, jarFile: String) {
    if (filesOnlyInJar.isEmpty())
        return

    println("Files found only in $jarFile")
    filesOnlyInJar.forEach({ name ->
        println(name)
    })
    println()
}

private fun print(filesThatDiffer: ArrayList<String>) {
    if (filesThatDiffer.isEmpty())
        return

    println("Files that differ")
    filesThatDiffer.forEach(::println)
}
