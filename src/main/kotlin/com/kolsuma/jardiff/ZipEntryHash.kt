package com.kolsuma.jardiff

import java.io.FileInputStream
import java.io.InputStream
import java.security.MessageDigest
import java.util.zip.ZipInputStream

fun hashes(jarFile: String): HashMap<String, ByteArray> {
    val zipInputStream = ZipInputStream(FileInputStream(jarFile))
    val hashes = HashMap<String, ByteArray>()

    while (true) {
        val entry = zipInputStream.nextEntry ?: break

        if (entry.isDirectory)
            continue

        hashes[entry.name] = hashOf(zipInputStream)
    }

    return hashes
}

private fun hashOf(inputStream: InputStream): ByteArray {
    val buffer = ByteArray(1024 * 1024)
    val digest = MessageDigest.getInstance("SHA-256")

    while (true) {
        val read = inputStream.read(buffer)
        if (read == -1) {
            break
        }
        digest.update(buffer, 0, read)
    }

    return digest.digest()
}